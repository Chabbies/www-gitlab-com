title: Paessler AG
cover_image: '/images/blogimages/paessler-case-study-image.png'
cover_title: |
  Paessler AG switched from Jenkins to GitLab and ramped up to 4x more releases
cover_description: |
  Helping customers monitor their entire IT infrastructure 24/7, Paessler AG keeps up with this fast-moving field with GitLab for version control and continuous delivery
twitter_image: '/images/blogimages/paessler-case-study-image.png'

customer_logo: 'images/case_study_logos/paessler-logo.svg'
customer_logo_css_class: brand-logo-wide
customer_industry: Network Software
customer_location: Nuremberg, Germany
customer_employees: 201-500
customer_overview: |
  Paessler AG is the company behind PRTG Network Monitor, an award-winning Unified Monitoring Solution. Helping IT professionals monitor their entire infrastructure around the clock, PRTG monitors all systems, devices, traffic and applications of your IT infrastructure.
customer_challenge: Connecting globally distributed R&D teams to implement DevOps
customer_solution: GitLab

key_benefits:
  - |
    Adopting Git for modern version control helped restore stability
  - |
    Accelerated from 3 major releases a year to continuous delivery and monthly releases
  - |
    GitLab pipelines test every single branch automatically when new code is
    committed, resulting in higher quality control and self-service QA

customer_stats:
  - stat: 90%
    label: of QA self-served
  - stat: 120x
    label: speed increase
  - stat: 15
    label: releases per day

customer_study_content:
  - title: the customer 
    subtitle: Providing round-the-clock monitoring for the enterprise
    content:
      - |
        Paessler AG’s [PRTG Network Monitor](https://www.paessler.com/prtg) is
        used by enterprises and organizations of all sizes and industries across
        more than 170 countries. With customers as diverse as London’s National
        Theatre, Fulham Football Club, universities, and healthcare networks,
        PRTG constantly monitors IT infrastructure, alerting administrators to
        problems with bandwidth, applications, networks, databases, and more,
        all before users are even aware of an issue.

  - title: the challenge
    subtitle: Instability and erratic performance
    content:
      - |
        In offering an all-encompassing monitoring service, it’s critical that
        PRTG moves fast to keep up with developments in every space. Paessler
        migrated their development of PRTG from Mercurial to GitLab primarily
        for version control purposes. A combination of a cumbersome, 7GB
        repository (the initial commit took place 20 years ago) and some bad
        practices (the absence of LFS in Mercurial means they had their binaries
        as part of the repository) led to major stability issues that made
        pulling, pushing and merging all work inconsistently
      - |
        “There were times where it just stopped working; you couldn’t pull and
        push anymore and you didn’t really know why,” said Konstantin Wolff,
        Infrastructure Engineer (PRTG Development).

  - title: the solution 
    subtitle: Modern version control and unprecedented QA automation with GitLab
    content:
      - |
        This instability prompted Paessler to seek out a Git solution. “A lot of
        people used Git at the company and just knew that it was faster, better,
        and what everybody else is using nowadays,” said Greg Campion, Senior
        Systems Administrator. “It’s also harder now to train somebody to use
        Mercurial because Git is so prevalent. So, when you hire a developer,
        they probably know Git but they may not know Mercurial.”
      - |
        After testing a number of version control systems, they chose GitLab for
        “the whole package.”  Adopting Git restored stability and helped Paessler
        ramp up their release cycle from three major releases a year to continuous
        delivery and monthly releases. But arguably the greatest outcome of their
        switch to GitLab has been the unanticipated effect on QA automation.

  - title:  the results
    subtitle: Stability, more frequent releases, and 120x boost for QA tasks
    content:
      - |
        Greg noticed the functionality and potential of GitLab pipelines, and
        adopted them for the cloud team he works on, and it’s since caught on
        all over the organization.
      - |
        “Before we had our build pipeline running with Jenkins,” explained
        Konstantin. “So, we have a develop, release, and master branch, which is
        finally released to the public. Every time you build a branch of those
        three, the outcome would have been installs on several VMs and then our
        test automation started in a separate process. At the end of the process
        you got an email that it worked, it didn’t work, where it failed, and
        stuff like that.”
      - |
        This sequential process, with feedback only available at the end, was
        only triggered automatically on the dev, release and master branches.
        A QA engineer had to perform some tasks to make this happen, around 10
        minutes, 6-7 times a day. If other branches needed testing they’d have
        to build them locally and run a test locally.
      - |
        The situation now could not be more different with the use of GitLab:
        “Every branch gets tested,” said Greg. “It’s built into the pipeline.
        As soon as you commit your code, the whole process kicks off to get it
        tested. Then you can go to the branch’s review app and have a running
        version of PRTG that you’ve just checked in code for, that’s already
        also been tested.” What this means in practice is higher quality control
        for their product and a significantly tighter feedback loop between
        developers and QA.
      - |
        “The amount of effort involved in actually getting to the newest version
        that you’re supposed to be testing, whether you’re a developer or a QA engineer,
        is minimized immensely.” The QA engineer’s tasks – about an hour a day
        in total – have been slashed to 30 seconds, a 120x speed increase.

  - blockquote: Every branch gets tested, it’s built into the pipeline. As soon as you commit your code, the whole process kicks off to get it tested. The amount of effort involved in actually getting to the newest version that you’re supposed to be testing, whether you’re a developer or a QA engineer, is minimized immensely.
    attribution: Greg Campion
    attribution_title: SENIOR SYSTEMS ADMINISTRATOR
  
  - content:
      - |
        This automation really pays dividends when something is wrong: if the
        tests fail, the pipeline fails, so the developer already knows something
        has gone awry, instead of waiting to hear that from QA. This immediate
        feedback now has developers at Paessler self-serving 90 percent of their
        QA.
      - |
        While it’s sometimes a challenge to drive widespread adoption of a new
        tool within an organization, the uptake of GitLab CI/CD at Paessler has
        been remarkable. “It really started catching on when people just saw our
        pipelines,” Greg said. “We actually had an internal learning session that
        was basically, ‘You show us yours, and we'll show you ours.’ Everybody
        showed off their pipelines just to see what they’re capable of, what
        people are doing with them, to get new ideas and stuff like that. It was
        actually a pretty successful, interesting session.”
      - |
        “GitLab is being used like crazy. And now that there’s a bunch of cool
        stuff going on in it, everybody wants to jump on the bandwagon.”
