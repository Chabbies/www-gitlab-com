require 'httparty'

PRIVATE_TOKEN = ENV['PRIVATE_TOKEN']

class GitLabInstance
  attr_reader :name, :endpoint

  def initialize(endpoint, private_token, name)
    @endpoint = "#{endpoint}/api/v4"
    @private_token = private_token
    @name = name
  end

  def call(path, params = '')
    url = @endpoint + path + params
    response = HTTParty.get(url, headers: headers)

    if response.ok?
      response
    else
      puts "#{response.code} while retrieving #{url}"

      []
    end
  end

  private

  def headers
    {
      'PRIVATE-TOKEN' => @private_token,
      'User-Agent' => 'www-gitlab-com'
    }
  end
end

class GitLabProject
  def initialize(id, instance)
    @id = id
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.call("/projects/#{@id}/milestones?state=active")
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def milestone(milestone_id)
    @instance.call("/projects/#{@id}/milestones/#{milestone_id}")
  end

  def milestone_direction_issues(milestone_id)
    @instance.call("/projects/#{@id}/issues", "?milestone=#{milestone_id}&labels=direction")
  end

  def wishlist_issues(label, not_label = nil)
    result = @instance.call("/projects/#{@id}/issues", "?labels=direction,#{label}&state=opened&per_page=100&sort=asc")
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result.select { |issue| issue['milestone'].nil? || issue['milestone']['title'] == 'Backlog' }
  end

  def product_vision_issues(label, not_label = nil)
    result = @instance.call("/projects/#{@id}/issues", "?labels=Product+Vision+2018,#{label}&per_page=100&sort=asc")
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result
  end

  def tier_issues(tier)
    @instance.call("/projects/#{@id}/issues", "?labels=direction,#{tier}&state=opened&per_page=100&sort=asc")
    # result = result.select { |issue| issue["milestone"] && issue["milestone"]["due_date"] }
  end

  def name
    project['name']
  end

  def web_url
    project['web_url']
  end

  def project
    @project ||= @instance.call("/projects/#{@id}")
  end

  def endpoint
    @instance.endpoint
  end
end

class GitLabGroup
  def initialize(id, instance)
    @id = id
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.call("/groups/#{@id}/milestones?state=active")
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def product_vision_epics(label, not_label = nil)
    result = @instance.call("/groups/#{@id}/epics", "?labels=Product+Vision+2018,#{label}&per_page=100&sort=asc")
    result = result.select { |epic| (epic['labels'] & not_label).empty? } if not_label
    result
  end

  def group
    @group ||= @instance.call("/groups/#{@id}")
  end
end

def issue_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
  output << "\n"
  output
end

def tier_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << " <kbd>#{issue['milestone']['title']}</kbd>" if issue['milestone']
  output << "\n"
  output
end

def product_vision_bullet(issue)
  output = "<i class=\"vision-item fa fa-#{'check-' if issue['state'] == 'closed'}square-o\" aria-hidden=\"true\"></i> [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
  output << "<br>\n"
  output
end

def epic_web_url(group, epic)
  "#{group.group['web_url']}/-/epics/#{epic['iid']}"
end

def product_vision_bullet_from_epic(group, epic)
  output = "<i class=\"vision-item fa fa-minus-square-o\" aria-hidden=\"true\"></i> [#{epic['title']}](#{epic_web_url(group, epic)})"
  output << ' <kbd>Starter</kbd>' if epic['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if epic['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if epic['labels'].include? 'GitLab Ultimate'
  output << "<br>\n"
  output
end

def generate_direction
  print 'Generating direction...'
  direction_output = ''
  issues = []

  edition.each do |project|
    milestones = project.milestones + gitlaborg.milestones
    milestones.sort_by! do |ms|
      Date.parse ms['due_date']
    end

    milestones.each do |ms|
      if ms['due_date'] && Date.parse(ms['due_date']) >= Date.today
        issues += project.milestone_direction_issues(ms['title'])
      end
    end
  end
  issues.group_by { |issue| issue['milestone']['title'] }.each do |title, group|
    direction_output << "#### #{title} \n\n"

    group.each do |issue|
      direction_output << issue_bullet(issue)
    end

    direction_output << "\n"
  end
  print "\n"

  direction_output
end

def edition
  @edition ||= begin
                 com = GitLabInstance.new('https://gitlab.com', PRIVATE_TOKEN, 'GitLab.com')
                 ce = GitLabProject.new('gitlab-org%2Fgitlab-ce', com)
                 ee = GitLabProject.new('gitlab-org%2Fgitlab-ee', com)
                 omnibus = GitLabProject.new('gitlab-org%2Fomnibus-gitlab', com)
                 [ce, ee, omnibus]
               end
end

def gitlaborg
  @group ||= begin
               com = GitLabInstance.new('https://gitlab.com', PRIVATE_TOKEN, 'GitLab.com')
               GitLabGroup.new('gitlab-org', com)
             end
end

def label_list(label, not_label = nil, editions = nil)
  output = ''

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.wishlist_issues(label, not_label)
    issues.each do |issue|
      output << issue_bullet(issue)
    end
  end
  output = "No current issues\n" if output.empty?
  output
end

def tier_list(label)
  output = ''
  issues = []

  edition.each do |project|
    issues += project.tier_issues(label)
  end
  issues.sort_by! do |issue|
    if issue.dig('milestone', 'due_date')
      Date.parse(issue['milestone']['due_date'])
    else
      Date.new(2050, 1, 1)
    end
  end
  issues.each do |issue|
    output << tier_bullet(issue)
  end

  output = "No current issues\n" if output.empty?
  output
end

def product_vision_list(label, not_label = nil, editions = nil)
  output = ''

  epics = gitlaborg.product_vision_epics(label, not_label)
  epics.each do |epic|
    output << product_vision_bullet_from_epic(gitlaborg, epic)
  end

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.product_vision_issues(label, not_label)
    issues.each do |issue|
      output << product_vision_bullet(issue)
    end
  end
  output = "No issues\n" if output.empty?
  output
end

def generate_wishlist
  print 'Generating wishlist...'
  output = {}

  ['chat commands', 'ci-build', 'code review', 'container registry', 'deploy', 'deliver', 'issue boards', 'issues', 'pages', 'pipeline', 'major wins', 'moderation', 'moonshots', 'open source', 'performance', 'Monitoring', 'service desk', 'test', 'usability', 'vcs for everything', 'wiki', 'HA', 'Cloud Native'].each do |label|
    output[label] = label_list(label)
  end
  output['CI/CD'] = label_list('CI/CD', ['ci-build', 'deploy', 'deliver', 'pages', 'pipeline', 'test', 'container registry', 'chat commands'])
  output['build'] = label_list('direction', ['HA', 'Cloud Native'], Array(edition[2]))
  print "\n"

  ['GitLab Starter', 'GitLab Premium', 'GitLab Ultimate'].each do |tier|
    output[tier] = tier_list(tier)
  end

  output
end

def generate_product_vision
  print 'Generating product vision...'
  output = {}

  ['project management', 'portfolio management', 'repository', 'code review', 'web ide', 'Security Products', 'devops:package', 'devops:release', 'application control panel', 'infrastructure configuration', 'operations', 'feature management', 'application performance monitoring', 'production monitoring', 'error tracking', 'logging', 'performance', 'Omnibus', 'Cloud Native'].each do |label|
    output[label] = product_vision_list(label)
  end
  output['ci'] = product_vision_list('devops:verify', ['Security Products'])

  print "\n"

  output
end

if $PROGRAM_NAME == __FILE__
  generate_direction
  generate_wishlist
  generate_product_vision
end
