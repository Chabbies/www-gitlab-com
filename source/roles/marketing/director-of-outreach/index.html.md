---
layout: job_page
title: "Director of Outreach"
---

As the Director of Developer Relations, you will be responsible for managing and building the team that focuses on supporting evangelists, committers, students, and open source projects.

## Responsibilities

1. Grow the number of evangelists that regularly give talks, write blog posts, and organize meetups about GitLab and concurrent DevOps.
1. Grow the number of committers that regularly contribute code to GitLab by making it easier and providing encouragement.
1. Grow the number of students using GitLab through a program for educational institutions.
1. Grow the number of active open source projects either self-hosting GitLab or using GitLab.com.
1. Ensure every time GitLab or something relevant to GitLab is discussed on social media there is a timely and thoughtful response.
1. Help our team create widely read content focussed on users of GitLab for our blog and YouTube channel.

## Reports

You will hire and train a global outreach team to grow the GitLab community:

1. Evangelist manager
1. Committer manager
1. Student manager
1. Open source projects manager
1. Community advocates (to respond on social media, we already have two people in that role today)
1. User content writer

## Requirements

* You were a full-time developer in a previous life but prefer to work with the developer community to improve experience and support through education.
* You have 5 years of experience leading developer relations or community advocacy programs, preferably open source in nature, and you have built a team before.
* You are creative. You’ve made people happy with your quirky campaigns.
* You give a great keynote and write a great blog and have videos and articles to prove it.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* You know how to run a marketing pipeline and can articulate how to use automation for the evangelist, committer, and student funnel.
* Excellent spoken and written English.
* Familiar with Git, Ruby, and GitLab.
* Analytical and data driven in your approach to building and nurturing communities.
* You are obsessed with making developers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Bonus points for having an existing network from a diverse set of communities and social media platforms.
* Media training and experience in communicating with journalists, bloggers and other media on a range of technical topics is a plus.
* You share our values, and work in accordance with those values.
