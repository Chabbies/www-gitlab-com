--- 
layout: markdown_page
title: "Department Structure"
---

## Department Structure

Below is a table showing the structure of GitLab departments as they exist in NetSuite. Please [check out the Team Page](/team/chart) to see our org chart.

| Sales | Cost of Sales | Marketing | Development | General & Administrative | GitLab.com | GitHost |
| :-----------: | :------: | :------------: | :-----------: | :------: | :------------: | :------------: |
| Sales Management | Customer Solutions | Corporate Marketing | Infrastructure       | PeopleOps | 
| Sales New Business Development | Customer Support  | Developer Relations    | Product Management      | Finance  |       |
| Sales Account Management | | Marketing & Sales Development | 
| Customer Success || Product Marketing | 
| Indirect Channel |