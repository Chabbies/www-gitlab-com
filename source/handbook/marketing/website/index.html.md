---
layout: markdown_page
title: "Website Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Objectives

Serve the needs and interests of our key audiences:

1. Users of GitLab: software developers and IT operations practicioners.
2. Buyers of GitLab: IT management, software architects, development leads.
3. Users of and contributors to OSS on gitlab.com.

Generate demand for GitLab by:

1. Showcasing the benefits of the most important GitLab features and how they can save time and money.
2. Compare GitLab vs competing products.
3. Provide customer case studies which illustrate 1 and 2.

## Scope

When referring to the GitLab marketing site, `docs.gitlab.com` `gitlab.com` and
the `about.gitlab.com/handbook` are not included.

## Definitions

### Topics
A topic is an industry trend, theme, or technology related to GitLab and our customers. For example, DevOps, GDPR, Containers, etc. Topic pages on our website educate the reader about the topic and share GitLab’s point of view while providing additional links to resources related to that topic. These pages are intended to attract search traffic.

Examples of other companies who have topic pages: 
- [https://www.redhat.com/en/topics/containers](https://www.redhat.com/en/topics/containers)
- [https://pivotal.io/containers](https://pivotal.io/containers) 
- [https://pivotal.io/topics](https://www.redhat.com/en/topics/containers)

### Solutions
A solution is a combination of products and services that solve a business problem. For example, accelerating software delivery, enabling remote teams, ensuring compliance, etc. Solution pages on our website show the application of GitLab capabilities and services to address a business problem while providing additional links to resources related to that solution. 

Examples of other companies who have solutions pages: 
- [https://www.redhat.com/en/challenges](https://www.redhat.com/en/challenges)

### Product section
The product section of our website has pages that describe what GitLab does and the value provided. The functionality of GitLab is ordered in a hirearchy with 4 levels: Stage, Categories, Capabilities, and Features. You can find details on the [Product Categories Handbook](/handbook/product/categories/)

- Stages relevant to users are listed on the [product overview page](/product).
- Categories relevant to users are list on the [product overview page](/product). 
- Capabilities are listed on the category page they belong to. Capabilities may also have their own landing page. 
- Features are listed in many places on the website: on the features page, the capabilities page they belong to, the pricing page, comparison pages, and the ROI calculator.

Examples of companies who have product/features pages: 
[https://mailchimp.com/features/](https://mailchimp.com/features/)
[https://www.groovehq.com/features](https://www.groovehq.com/features)

### Overlap 

Similiar content can appear as a topic, solution, and in the product section with different emphasises on each page. For example continuous integration:

- A topic page: `/continuous-integration` would talk about what CI is at a functional level. 
- A solutions: `/solutions/continuous-integration`. would talk about why CI is important for businesses to adopt.
- A category page `/product/continuous-integration` would talk about the capabilities and features that are part of GitLab's CI functionality and the value it has.

## Ownership and responsibilities

The marketing site is an important part of our company requiring close coordination and collaboration across multiple teams. Below details which functional group is primarily responsible for which areas of the marketing site.

### Marketing Site Product Manager

[Luke Babb](/team/#lukebabb) (interim) is responsible for
scheduling tasks and allocating various team members to accomplish tasks.

Track projects on the [Website Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137).

### Content Marketing

- Copy for company and audience focused pages:
  - Homepage
  - Press page
  - Company pages
  - Community pages
  - Topics
  - Customers and case study pages
- Editorial direction of the blog
- Design input for the above

### Product Marketing

- Copy for product focused pages:
  - Product
  - Solutions
  - Pricing
- Sales enablement pages (e.g. comparison pages, ROI calculator)
- Design input for the above

### Growth Marketing

- Build and implement online growth strategy:
  - Search Engine Optimization
  - Paid search and social
  - Conversion Rate Optimization (CRO) and a/b and multivariate testing
-  Web analytics, tracking, and reporting 
-  User journey optimization

### Design

- Create design mockups based on stakeholder input.
- Iterate on the design mockups until goals and objectives of the page are met.
- Produce a final [Sketch spec preview](https://gitlab.com/gitlab-com/marketing/tree/master#repository-superpowers-) and artwork assets for the Frontend Development team to implement.
- Review front-end implementation (merge request) and provide feedback and guidance to improve the design.

### Frontend Development

- Implement final approved design mockup.
- Pixel-push the design based on feedback from the Design team to match the final design mockup and vision for the page.

### All Product Managers

- Updating the *technical feature comparison tables* on
  `about.gitlab.com/comparison` and `about.gitlab.com/products` for the products
  they manage e.g GitLab column and competitor columns with list of feature
  names.

### Technical Writing

- Technical content on the `about.gitlab.com/installation` page (not the design
  and UX of this page which shoud be shared with the marketing site).
- Assist the Product Managers with the backlog of missing *technical feature
  comparison tables* on `about.gitlab.com/comparison` and
  `about.gitlab.com/products` e.g GitLab CI vs Jenkins.
  The Technical Writing team own the tasks from the backlog but will still have
  to ask the relevant Product Manger for content input as they know the
  feature's competitive landscape better than anyone.
- With regards to documentation the Technical Writing team is focusing on
  up to date and feature complete written documentation. No video content is
  planned for now.
